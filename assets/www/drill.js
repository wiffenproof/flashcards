function DrillCtrl($scope) {
  $scope.solved = 0;
  $scope.mistakes = 0;
  $scope.maxDomain = 10;
  // The numbers that can be used in a multiplication problem.
  $scope.domain = generateDomain();

  $scope.problem = generateRandomCard();

  $scope.imageIndex = 0;
  $scope.maxImageIndex = 13; // 0..13

  function generateDomain() {
    var domain = [];
    for (var i = 1; i <= $scope.maxDomain; i++) {
        domain.push({index: i, enabled: true});
    }
    return domain;
  }

  function isEnabled(x) {
    return $scope.domain[x-1].enabled;
  }

  $scope.domainChanged = function () {
    var problem = $scope.problem;
    var allowed = isEnabled(problem.x) || isEnabled(problem.y);
    if (! allowed) {
        $scope.problem = generateRandomCard();
    }
  }

  function generateRandomCard() {
    var domain = $scope.domain;
    var allowed = [];
    for (var i = 0; i < domain.length; i++) {
        var d = domain[i];
        if (d.enabled) {
            allowed.push(d.index);
        }
    }
    if (allowed.length == 0) {
        allowed = [1];
    }
    var x = allowed[randomInt(allowed.length)];
    var y = randomRange(1, $scope.maxDomain + 1);

    if (randomInt(2) == 1) {
        var temp = x;
        x = y;
        y = temp;
    }
    return generateCard(x, y);
  }

  function generateCard(x, y) {
    var front = x + ' * ' + y;
    var answer = x * y;
	var choiceCount = 5;
  	var choices = [];
  	choices.push(answer);
  	var variationRange = 14;
  	var begin = Math.max(1, answer + randomInt(-variationRange, 0));
  	var end = begin + variationRange;
  	for (var k = 1; k < choiceCount; k++) {
  		var choice;
  		do {
  			choice = randomRange(begin, end);
  		} while (choices.indexOf(choice) != -1);
  		choices.push(choice);
  	}
  	choices.sort(function(a,b) { return a - b;});
    var card = {front: front, x: x, y: y, answer: answer, choices: choices};
	return card;
  }

  function randomInt(end) {
  	return Math.floor(Math.random() * end);
  }

  function randomRange(begin, end) {
  	return begin + randomInt(end - begin);
  }
  function randomInsert(list, item) {
  	var randomIndex = randomInt(list.length + 1);
    list.splice(randomIndex, 0, item);
  }

  $scope.answer = function(event) {
  	var target = event.target;
  	var answer = target.value;
  	if (answer == $scope.problem.answer) {
        $scope.solved += 1;
        $scope.problem = generateRandomCard();
        if ($scope.solved % 5 == 0) {
            $scope.imageIndex = ($scope.imageIndex + 1) % ($scope.maxImageIndex + 1);
        }
  	} else {
        $scope.mistakes += 1;
  		target.value = 'xxx';
        target.disabled = true;
  		randomInsert($scope.problems, generateCard($scope.problem.front, $scope.problem.answer));
  	}
  }

  $scope.reset = function() {
    $scope.solved = 0;
    $scope.mistakes = 0;
    $scope.imageIndex = 0;
    $scope.problem = generateRandomCard();
  }
}

