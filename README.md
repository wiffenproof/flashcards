A simple Math flashcard app I wrote for my third-grade daughters to learn
multiplication.

Worked pretty well for one of them. For the other it turned out to be better
to use traditional paper-based memorize-one-line-of-the-times-table-at-a-time
drill.

This app is packaged as an Android PhoneGap app, so it's not that easy to build.

If you want a rough idea of what the app is like, you can also run it on
a computer using Python's built-in web server:

    $ cd assets/www
    $ python -m SimpleHTTPServer

And then browse localhost:8000 in a modern web browser like Chrome.
